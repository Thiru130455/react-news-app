import React, {
    Component
} from 'react'
class NewsCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            imgurl: 'https://cdn.pixabay.com/photo/2015/07/02/09/52/interior-design-828545_960_720.jpg'
        }
    }

    isEven = (n) => {
        n = Number(n);
        return n === 0 || !!(n && !(n % 2));
    }

    isOdd = (n) => {
        return this.isEven(Number(n) + 1);
    }


    render() {
        const data = this.props.data;
        return (
            <>
                {data.map((elem, index) => {
                    return (
                        <div>
                            {this.isOdd(index) === false ?
                                <div className="blog-card" key={index}>
                                    <div className="meta">
                                        <div className="photo" style={{
                                            backgroundImage: `url(${elem.urlToImage})`,

                                        }}>

                                        </div>
                                        <ul className="details">
                                            <li className="author"><a >{elem.author}</a></li>
                                            <li className="date">{elem.publishedAt}</li>
                                            <li className="tags">
                                                <ul>
                                                    <li><a >C++</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="description">
                                        <h1>{elem.title}</h1>
                                        <h2>{elem.description}</h2>
                                        <p> {elem.content}</p>
                                        <p className="read-more">
                                            <a href={elem.url}>Read More</a>
                                        </p>
                                    </div>
                                </div> :
                                <div className="blog-card alt" key={index}>
                                    <div className="meta">
                                        <div className="photo" style={{ backgroundImage: `url(${elem.urlToImage})` }}></div>
                                        <ul className="details">
                                            <li className="author"><a >{elem.author}</a></li>
                                            <li className="date">{elem.publishedAt}</li>
                                            <li className="tags">
                                                <ul>
                                                    <li><a >Buy</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="description">
                                        <h1>{elem.title}</h1>
                                        <h2>{elem.description}</h2>
                                        <p>{elem.content}</p>
                                        <p className="read-more">
                                            <a href={elem.url}>Read More</a>
                                        </p>
                                    </div>
                                </div>}
                        </div>
                    )
                })}

            </>)
    }
}

export default NewsCard;


