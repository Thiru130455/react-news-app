import React, { Component } from 'react'
import { Tabs } from 'antd';
import '../assets/tab.scss'
import NewsCard from './NewsCard';
const { TabPane } = Tabs;
class NewsComponent extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            apiKey:"05430b3ead274409addb50fa8705ed5d",
            date: new Date(),
            newsList:[]
        }
        
        
        
        
    }
    
    getNews(key) {
        debugger
        console.log(key);
        switch (parseInt(key)) {
            case 1:this.getNewsArray('apple');
                break;
           case 2: this.getNewsArray('india')
                break;
           case 3:this.getNewsArray('cricket')
                break;
           case 4: this.getNewsArray('oscar')
                break;
           case 5: this.getNewsArray('technology')
                break;
           case 6: this.getNewsArray('international')
                break;
        
            default:
                break;
        }
    }

    componentDidMount(){
    this.getNewsArray('apple');

    }

    getNewsArray(newsName) {
        const fromDate = this.getDate();
        const toDate = this.getDate();
        const requestOptions = {
            method: 'GET',
        };
        const url = 'https://newsapi.org/v2/everything?q='
            + newsName + '&from=' + fromDate + '&to=' + toDate + '&sortBy=popularity&apiKey='
            + this.state.apiKey;
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ newsList: data.articles });
            });
    }

    getDate() {
        var date ;
        date = this.state.date.getUTCFullYear()+'-'+(this.state.date.getMonth()+1)+'-'+this.state.date.getDate();
        return date;
    }

    render() {
        return (
            <div>
                <Tabs defaultActiveKey="1" onChange={(e)=>this.getNews(e)}>
                    <TabPane tab="Apple" key="1">
                        <NewsCard data={this.state.newsList}/>
                     </TabPane>
                    <TabPane tab="India" key="2">
                    <NewsCard data={this.state.newsList}/>
                </TabPane>
                    <TabPane tab="Cricket" key="3">
                        <NewsCard data={this.state.newsList}/>
                   </TabPane>
                   <TabPane tab="Oscar 2k19" key="4">
                        <NewsCard data={this.state.newsList}/>
                   </TabPane>
                   <TabPane tab="Technology" key="5">
                        <NewsCard data={this.state.newsList}/>
                   </TabPane>
                   <TabPane tab="International" key="6">
                        <NewsCard data={this.state.newsList}/>
                   </TabPane>
                </Tabs>
            </div>
        )
    }
}

export default NewsComponent
