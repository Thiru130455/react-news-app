import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import NewsComponent from './pages/NewsComponent';

function App() {
  return (
    <div className="App">
      <NewsComponent/>
    </div>
  );
}

export default App;
